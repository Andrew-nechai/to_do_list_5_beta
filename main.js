window.onload = function () {

	var check_svg = '<img src="img/check.svg" alt="" id="check">';
	var uncheck_svg = '<img src="img/uncheck.svg" alt="">';
	var REMOVE_SVG = '<img src="img/delete.svg" alt="" class="clear" id="clear">';
	var mass = [];
	var mass_of_todos = [];
	var list = document.getElementById('todo');	

	let all = 0, active = 0, complete = 0;

	// document.getElementById("Selected1").onclick = function() {
	// 	if (mass.length == 0) return 0;	
	// 	if (select == 0) alert("Выберите нужные элементы");
	// 	select == 0 ? select = 1 : select = 0;
	// }

	// document.getElementById("Selected2").onclick = function() {
	// 	if (select == 1){
	// 		if (confirm("Вы действительно хотите удалить выбранные дела?")){
	// 			for (let i=0; i < mass.length; i++){
	// 				if (mass[i].select == true) {
	// 					mass[i].existence = false;
	// 				}
	// 			}
	// 			let x = document.getElementById("todo");
	// 			let mass_length = mass.length;
	// 			for(let i=0; i < mass_length; i++){
	// 				if (x.childNodes[i].classList.contains("selected")){
	// 					x.childNodes[i].parentNode.removeChild(x.childNodes[i]);
	// 					i--;
	// 					mass_length--;
	// 				}
	// 			}
	// 		} else {
	// 			alert("Действие отменено");
	// 		}
	// 		delete_from_array();
	// 	}
	// 	select = 0;
	// }

//добавление дела на страницу

	document.onkeyup = function(key) {
		if (key.keyCode == 13) add_deals();
	}

	document.getElementById('add').onclick = function() {add_deals()};

	function add_deals() {	
		var task = document.getElementById('input').value;
		if (!task) return 0;
		addtask(task);
		reset_all_styles();
		if (coll_style_of_todo2() == 1){
			document.getElementById('clear-items').innerHTML = "Clear Selected";
		}
		document.getElementById('input').value = '';
	};

//сброс стилей дел и стилей кнопок обработки дел

	function reset_all_styles(){	
		length_of_mass = mass.length;
		for (let i = 0; i < length_of_mass; i++){
			let x = document.getElementById("todo").childNodes[i];
			if (x.classList.contains("style-of-todo1")) {
				x.classList.remove('style-of-todo1');
			}
		}
		all = 0;
		active = 0;
		complete = 0;
		reset_white_shadow();
		document.getElementById('items_left').innerHTML = "Items Left: " + col_of_mass_active();
		document.getElementById('clear-items').innerHTML = "Clear Items";
	}

//функция удаления элементов из массива

	function delete_from_array() {
		let mass_length = mass.length;
		for (let i = 0; i < mass_length; i++){
			if (mass[i].existence == false) {
				mass.splice(i,1);				//удаление элемента с позиции i 1 элемент
				mass_of_todos.splice(i,1);
				i--;
				mass_length--;
			}
		}
	}


//создание дела и занесение соотв-х значений в массивы

	function addtask(smth) {
		var item_add = {};
		var task = document.getElementById('input').value;
		item_add.existence = true;
		item_add.checked = false; 
		item_add.task = smth;
		let text_of_todo = document.createElement('span');
		text_of_todo.innerText = smth;	
		var but_check = document.createElement('button');
		but_check.classList.add('check'); 
		but_check.innerHTML = check_svg;
		var item = document.createElement('li');
		item.classList.add("style-of-todo");
		but_check.onclick = function() {
			but_check.classList.toggle("check");
			but_check.classList.toggle("uncheck");
			item_add.checked = (but_check.classList.value == "check") ? "check":"uncheck";
			let y = this;
			if (but_check.classList == "uncheck") {
				y.style.textDecoration = 'line-through';
			} else {
				y.style.textDecoration = 'none';
			}
			if (but_check.classList.contains("uncheck")) {
		 		but_check.innerHTML = uncheck_svg; 
		 		item_add.checked = true;
			} else { 
				if (but_check.classList.contains("check")) {
					but_check.innerHTML = check_svg;
					item_add.checked = false;
				}
			}
			item.classList.toggle("style-of-todo2");
			reset_all_styles();
		}
		item.onclick = function() {	
			this.classList.toggle("style-of-todo2");
			reset_all_styles();
			if (coll_style_of_todo2() == 1){
				document.getElementById('clear-items').innerHTML = "Clear Selected";
			}
		}
		var but_remove = document.createElement('button');
		but_remove.classList.add('clear');
		but_remove.innerHTML = REMOVE_SVG;
		but_remove.onclick = function () {	
			reset_all_styles();
			var ul = document.getElementById('todo');
			var trash = this.parentNode; 
			item_add.existence = false;
			ul.removeChild(trash);
			delete_from_array();
		};
		item.appendChild(but_check);
		item.appendChild(text_of_todo);
		item.appendChild(but_remove);
		mass.push(item_add);		
		mass_of_todos.push(item);
		list.insertBefore(item, list.firstChild);
	}

// нажатие на кнопку All

	document.getElementById('all').onclick = function(){
		if (mass.length == 0) return (0);
		(all == 1) ? all=0 : all=1;
		reset_style_of_todo2();
		reset_styles_all();
		reset_white_shadow();
		document.getElementById('clear-items').innerHTML = "Clear All";
		for (let i = 0; i < mass_of_todos.length; i++){
		 	 let x = document.getElementById("todo").childNodes[i];
		 	 x.classList.toggle("style-of-todo1");
		 }
	};

//нажатие на кнопку Active


	document.getElementById('active').onclick = function(){
		let e = 0;
		for (let i=0; i<mass.length; i++){
			let x = document.getElementById("todo").childNodes[i];
			if(x.firstChild.className == "check") e++;
		}
		if (e == 0) return 0;
		(active == 1) ? active = 0 : active = 1;
		reset_style_of_todo2();
		reset_styles_active();
		reset_white_shadow();
		for (let i = 0; i < mass.length; i++){
		 	 let x = document.getElementById("todo").childNodes[i];
		 	 if (x.childNodes[0].classList.contains("check")) {
		 	 	x.classList.toggle("style-of-todo1");
		 	 }
		}
		document.getElementById('clear-items').innerHTML = "Clear Active";
	};

//нажатие на кнопку Complete

	document.getElementById('complete').onclick = function(){
		let e = 0;
		for (let i=0; i<mass.length; i++){
			let x = document.getElementById("todo").childNodes[i];
			if(x.firstChild.className == "uncheck") e++;
		}
		if (e == 0) return 0;
		(complete == 1) ? complete = 0 : complete = 1;
		reset_style_of_todo2();
		reset_styles_complete();
		reset_white_shadow();
		for (let i = 0; i < mass.length; i++){
		 	 let x = document.getElementById("todo").childNodes[i];
		 	 if (x.childNodes[0].classList.contains("uncheck")) {
		 	 	x.classList.toggle("style-of-todo1");
		 	 }
		 }
		document.getElementById('clear-items').innerHTML = "Clear Complete";
	};

//сброс выделения кнопок белой тенью
	function reset_white_shadow(){
		if (active == 1){
			document.getElementById('active').className = "button_selected";
			document.getElementById('all').className = "";
			document.getElementById('complete').className = "";
			return 0;
		}
		if (all == 1){
			document.getElementById('active').className = "";
			document.getElementById('all').className = "button_selected";
			document.getElementById('complete').className = "";
			return 0;
		} 
		if (complete == 1){
			document.getElementById('active').className = "";
			document.getElementById('all').className = "";
			document.getElementById('complete').className = "button_selected";
			return 0;
		} 

		if (all == 0 && active == 0 && complete == 0) {
			document.getElementById('active').className = "";
			document.getElementById('all').className = "";
			document.getElementById('complete').className = "";
		}
	}

//удаление 

	document.getElementById('clear-items').onclick = function(){
		if ((all == 1 || active == 1 || complete == 1) && (mass.length != 0)){
			if (confirm("Вы уверены, что хотите удалить выбранные дела?")){
				delete_from_array();
				if (document.getElementById('clear-items').innerHTML == "Clear All") {clear_all();}
				if (document.getElementById('clear-items').innerHTML == "Clear Active") {clear_active();}
				if (document.getElementById('clear-items').innerHTML == "Clear Complete") {clear_complete();}
				delete_from_array();
				reset_all_styles();
			} else 
				{
					reset_all_styles();
					alert("Действие отменено!");
				}
		} else {
			if (coll_style_of_todo2() == 1){
				if (confirm("Вы уверены, что хотите удалить выбранные дела?")){
					let t = document.getElementsByClassName("style-of-todo2");
					let i = 0;
					while (t[i]){
						t[i].childNodes[0].classList.add("uncheckedd");
						i++;
					}
					let x = document.getElementsByClassName("uncheckedd");
					while (x[0]){
						x[0].parentNode.parentNode.removeChild(x[0].parentNode);
					}
					for (let i = 0; i < mass_of_todos.length;i++){
						if (mass_of_todos[i].childNodes[0].classList.contains("uncheckedd")){
							mass[i].existence = false;
						}
					}
					delete_from_array();
				} else alert("Действие отменено!");
			}
		}
		document.getElementById('items_left').innerHTML = "Items Left: " + col_of_mass_active();
		document.getElementById('clear-items').innerHTML = "Clear Items";
		console.log(mass);
		console.log(mass_of_todos);
	}

//Функция возврата стилей для дел(после повторного нажатия на кнопки управления)

	function reset_style_of_todo2(){
		for (let i = 0; i < mass_of_todos.length; i++){
			let x = document.getElementById("todo").childNodes[i];
			if (x.classList.contains("style-of-todo2")) {
				x.classList.remove('style-of-todo2');
			}
		}
	}

	function coll_style_of_todo2(){
		for (let i = 0; i < mass_of_todos.length; i++){
			let x = document.getElementById("todo").childNodes[i];
			if (x.classList.contains("style-of-todo2")) {
				return 1;
			}
		}
		return 0;
	}



	function reset_styles_all(){
		if (active == 1 || complete == 1){
		for (let i = 0; i < mass_of_todos.length; i++){
			let x = document.getElementById("todo").childNodes[i];
			if (x.className == ("style-of-todo style-of-todo1")) {
				x.className = ("style-of-todo");
			}
		}
		active = 0;
		complete = 0;
		}
	}

	function reset_styles_active(){
		if (all == 1 || complete == 1){
			for (let i = 0; i < mass.length; i++){
				let x = document.getElementById("todo").childNodes[i];
				if (x.className == ("style-of-todo style-of-todo1")) {
					x.className = "style-of-todo";
				}
			}
		all = 0;
		complete = 0;
		}
	}

	function reset_styles_complete(){
		if (all == 1 || active == 1){
			for (let i = 0; i < mass.length; i++){
				let x = document.getElementById("todo").childNodes[i];
				if (x.className == "style-of-todo style-of-todo1") {
					x.className = "style-of-todo";
			    }
			}
		all = 0;
		active = 0;
		}	
	}

//Функция подсчета активных заданий 

	function col_of_mass_active(){
		var x = 0;
		for (let i = 0; i < mass.length; i++) {
			if ((mass[i].checked == false) && (mass[i].existence == true)) {
				x += 1;
			}
		}
		return x;
	}


//Функция подсчета неактивных заданий

	function col_of_mass_complete(){
		var x = 0;
		for (let i = 0; i < mass.length; i++) {
			if ((mass[i].checked == true) && (mass[i].existence == true)) {
				x += 1;
			}
		}	
		return x;

	}

//функция удаления всех дел 

	function clear_all() {
		let x = document.getElementsByClassName("check");
		let y = document.getElementsByClassName("uncheck");
			while (y[0]){
				let z = y[0].parentNode.parentNode;
				z.removeChild(y[0].parentNode);
			}
			while (x[0]){
				let y = x[0].parentNode.parentNode;
				y.removeChild(x[0].parentNode);
			}
			mass = [];
			mass_of_todos = [];
	}

//функция удаления активных дел

	function clear_active() {
		let x = document.getElementsByClassName("check");
		let y = document.getElementById("todo");
		while (x[0]){
			let y = x[0].parentNode.parentNode;
			y.removeChild(x[0].parentNode);
		}
		let u;
		for (let i = 0; i < mass.length;i++){
			u = mass_of_todos[i].childNodes[0];
			if (u.classList.value == "check"){
				mass[i].existence = false;
			}
		}
	}

//функция удаления выполненных дел

	function clear_complete() {
		let x = document.getElementsByClassName("uncheck");
		let y = document.getElementById("todo");
		while (x[0]){
			y.removeChild(x[0].parentNode);
		}
		for (let i = 0; i < mass.length;i++){
			if (mass_of_todos[i].childNodes[0].classList.value == "uncheck"){
				mass[i].existence = false;
			}
		}
	}
}